import csv
import os
from collections import defaultdict
import sys
import re
import numpy as np
import random
import math
import time

from itertools import cycle
from scipy import interp
import sklearn.datasets
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier
from sklearn import metrics
from sklearn.model_selection import GridSearchCV
from sklearn.externals import joblib
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import accuracy_score, confusion_matrix, roc_curve, classification_report, precision_score, recall_score, f1_score, auc
import pickle
import matplotlib.pyplot as plt

def plot_graph(scores):
	list_accuracy = []
	for score in scores:
		list_accuracy.append(score)
	plt.figure(figsize=(10, 6))
	plt.plot(range(0, len(list_accuracy)), list_accuracy, color='green', linestyle='solid', marker='*', markerfacecolor='orange',
				markersize=10)
	plt.title('Accuracy vs Parameter K')
	plt.xlabel('Parameter K')
	plt.ylabel('Accuracy')
	plt.show()

def roc_construct(y_test, y_score):
	n_classes = 5
	fpr = dict()
	tpr = dict()
	roc_auc = dict()
	for i in range(n_classes):
		fpr[i], tpr[i], _ = metrics.roc_curve(y_test[:, i], y_score[:, i])
		roc_auc[i] = metrics.auc(fpr[i], tpr[i])

	# Computing micro-average ROC curve and ROC area i.e AUC

	fpr["micro"], tpr["micro"], _ = metrics.roc_curve(y_test.ravel(), y_score.ravel())
	roc_auc["micro"] = metrics.auc(fpr["micro"], tpr["micro"])

	# Ploting ROC curve for a specific class/label

	plt.figure()
	lw = 2
	plt.plot(fpr[2], tpr[2], color='darkorange',
			 lw=lw, label='ROC curve (area = %0.2f)' % roc_auc[2])
	plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
	plt.xlim([0.0, 1.0])
	plt.ylim([0.0, 1.05])
	plt.xlabel('False Positive Rate')
	plt.ylabel('True Positive Rate')
	plt.title('Receiver operating characteristic DT')
	plt.legend(loc="lower right")
	plt.show()

	# Aggregating all false positive rates (fpr)
	all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))

	# Interpolating ROC curves
	mean_tpr = np.zeros_like(all_fpr)
	for i in range(n_classes):
		mean_tpr += interp(all_fpr, fpr[i], tpr[i])

	# Averaging it and computing AUC
	mean_tpr /= n_classes

	fpr["macro"] = all_fpr
	tpr["macro"] = mean_tpr
	roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

	# Ploting ROC curves all of them
	plt.figure()
	plt.plot(fpr["micro"], tpr["micro"],
			 label='micro-average ROC curve (area = {0:0.2f})'
				   ''.format(roc_auc["micro"]),
			 color='deeppink', linestyle=':', linewidth=4)

	plt.plot(fpr["macro"], tpr["macro"],
			 label='macro-average ROC curve (area = {0:0.2f})'
				   ''.format(roc_auc["macro"]),
			 color='navy', linestyle=':', linewidth=4)

	for i in range(n_classes):
		plt.plot(fpr[i], tpr[i], lw=lw,
				 label='ROC curve of class {0} (area = {1:0.2f})'
					   ''.format(i, roc_auc[i]))

	plt.plot([0, 1], [0, 1], 'k--', lw=lw)
	plt.xlim([0.0, 1.0])
	plt.ylim([0.0, 1.05])
	plt.xlabel('False Positive Rate')
	plt.ylabel('True Positive Rate')
	plt.title('Some extension of Receiver operating characteristic to multi-class DT')
	plt.legend(loc="lower right")
	plt.show()

def build_data_cv(datafile, trait_number):
	"""
	Loads data
	"""

	with open(datafile, "rb") as csvf:
		csvreader = csv.reader(csvf,delimiter=',',quotechar='"')
		data = []
		target = np.array([])

		for index, line in enumerate(csvreader):

			# escape header
			if index < 1:
				continue

			document = unicode(line[1], errors='replace')

			data.append(document)

			index_of_trait = trait_number + 2

			if line[index_of_trait].lower()=='y':
				target = np.append(target, 1.0)
			else:
				target = np.append(target, 0.0)

		dataset = sklearn.datasets.base.Bunch(data=data, target=target)
		dataset.target_names = ["positive", "negative"]

		return dataset

def svm_grid(dataset, docs_new):
	X_train, X_test, y_train, y_test = train_test_split(dataset.data, dataset.target, test_size=0.2, random_state=0)

	clf = Pipeline([('vect', CountVectorizer(decode_error='replace')),
					('tfidf', TfidfTransformer()),
					('clf', SGDClassifier(loss='hinge', penalty='l2',
										  alpha=1e-3, random_state=42,
										  max_iter=5, tol=None)),
					])
	# clf.fit(X_train, y_train)

	parameters = {'vect__ngram_range': [(1, 1), (1, 2)],
				  'tfidf__use_idf': (True, False),
				  'clf__alpha': (1e-2, 1e-3),
				  }
	start_time_grid_svm = time.time()
	gs_clf = GridSearchCV(clf, parameters, n_jobs=-1)

	# fit the model
	gs_clf.fit(X_train, y_train)

	# simple test score
	# print clf.score(X_test, y_test)

	# 10-fold cross-validation score
	scores = cross_val_score(gs_clf, dataset.data, dataset.target, cv=10)
	print("\nRuntime For gridsearch SVM : " + str(time.time() - start_time_grid_svm))
	print("Accuracy: %0.4f (+/- %0.4f)" % (scores.mean(), scores.std() * 2))
	#print(gs_clf.cv_results_)

	start_time_val_svm = time.time()

	y_pred = gs_clf.best_estimator_.predict(X_test)

	print("\nRuntime For validation SVM : " + str(time.time() - start_time_val_svm))

	print("\n\nMetrics are here : \n\n")
	print(classification_report(y_pred, y_test))
	print("\n\nConfusion Matrix : \n")
	print(confusion_matrix(y_test, y_pred))
	print("\n\n Precision score: ")
	print(precision_score(y_pred, y_test))
	print("\nRecall score: ")
	print(recall_score(y_test, y_pred))
	print("\nF1 score: ")
	print(f1_score(y_test, y_pred))
	#print("\nROC Curve: ")
	#roc_construct(y_test, y_pred)
	print("\nAccuracy graph: ")
	for score in scores:
		print score
	plot_graph(scores)

	# Export the classifier to a file
	joblib.dump(gs_clf, 'svm_gs_' + selected_trait + '.joblib')

	# docs_new = ["""
	# 			I have to go to the SI unit and then the Crew meeting and come home, shower, get ready, and meet Ann and Chad for dinner. I should have some time tonight to do homework. I'm still tired from last night, maybe I can take a nap sometime today. Otherwise, I can always sleep in tomorrow. Having only afternoon classes will be interesting. My lab might be hard because it's at night, but it seems really lax. It's such a difference from the chem labs. I'm so glad it doesn't seem hard at all. I hope my physics lab is easy as well. One can only hope. Not having lab the first week is nice. It worked out well so I could be home to help my parents help move our stuff. It's such a pain to have to move when school is already in session. I'm a little shocked the manager didn't have us sign the lease before we moved in. I hope that's not an indication of how much she cares about the renters. At least there's a lady downstairs who has lived here for 20 years that knows everything about this place. It's so nice having an apartment right next to a bus stop. I think it's fate we moved in here. After all the hassles we've had it looks like its going to be worth it. I still have to hang up all my clothes, but everything else is done. I can't forget to meet Dan at 10am on Monday in the PCL to work on physics homework. It's 6 pages long. I hope it's as easy as the first homework. It still took a while to finish though. From 7pm to 1am. This time I'll start earlier. I'm glad that my sister is coming in town, but she picked a bad weekend. I have a lot of reading to do and 2 tests to study for. I need to start seriously studying the PCAT. I can't believe I almost missed the deadline on that again. This is ridiculous. I hope I get into UT Austin. That's the only place I'd really like to go. Except maybe UCSF or University of Washington. UCSF is probably too expensive. I'd want to live in San Fran when I have money so really I only want to got o Pharmacy school in Austin or Seattle. The only thing about Seattle is I hope Sylvia has gotten cleaner. I know she never cooks anymore, so probably. We'd have to get a bigger place than what she has now and I might just have to leave all my stuff here and buy new stuff. That's going to be expensive. I hope whatever happens, Mom and Dad aren't disappointed in me. They need a web page for PAC so I know when I need to get in line and for what. The draw system for the football team is so much more efficient, even though they don't give out all the information I need either. Maybe next year I won't buy either. It's so much of a pain. I don't even feel like going. I bet that's how they make their money. I don't know about the plays, but at the games students get crappy seats. I know they were trying to improve that, I wonder if that went through. Probably not. As a whole, UT just cares about money and not the poor students. That's why alumni get everything. Good luck to the ""sick"" staff."
	# 		 """]

	start_time_test_svm = time.time()
	predicted = gs_clf.predict(docs_new)
	print("\nRuntime For testing SVM : " + str(time.time() - start_time_test_svm))
	print("Predicted Value : ")
	print predicted

def nbc_grid(dataset, docs_new):
	X_train, X_test, y_train, y_test = train_test_split(dataset.data, dataset.target, test_size=0.2, random_state=0)

	clf = Pipeline([('vect', CountVectorizer(decode_error='replace')),
					('tfidf', TfidfTransformer()),
					('clf', MultinomialNB()),
					])
	# clf.fit(X_train, y_train)

	parameters = {'vect__ngram_range': [(1, 1), (1, 2)],
				  'tfidf__use_idf': (True, False),
				  'clf__alpha': (1e-2, 1e-3),
				  }

	start_time_grid_nbc = time.time()
	gs_clf = GridSearchCV(clf, parameters, n_jobs=-1)

	# fit the model
	gs_clf.fit(X_train, y_train)

	# simple test score
	# print clf.score(X_test, y_test)

	# 10-fold cross-validation score
	scores = cross_val_score(gs_clf, dataset.data, dataset.target, cv=10)
	print("\nRuntime For grid search NBC : " + str(time.time() - start_time_grid_nbc))

	print("Accuracy: %0.4f (+/- %0.4f)" % (scores.mean(), scores.std() * 2))

	start_time_val_nbc = time.time()
	y_pred = gs_clf.best_estimator_.predict(X_test)
	print("\nRuntime For validation NBC : " + str(time.time() - start_time_val_nbc))

	print("\n\nMetrics are here : \n\n")
	print(classification_report(y_pred, y_test))
	print("\n\nConfusion Matrix : \n")
	print(confusion_matrix(y_test, y_pred))
	print("\n\n Precision score: ")
	print(precision_score(y_pred, y_test))
	print("\nRecall score: ")
	print(recall_score(y_test, y_pred))
	print("\nF1 score: ")
	print(f1_score(y_test, y_pred))
	print("\nAccuracy graph: ")
	plot_graph(scores)
	#print("\nROC Curve: ")
	#roc_construct(y_test, y_pred)
	joblib.dump(gs_clf, 'nb_' + selected_trait + '.joblib')

	print "______________________"

	# single entity test
	# docs_new = ["""
	#	I have to go to the SI unit and then the Crew meeting and come home, shower, get ready, and meet Ann and Chad for dinner. I should have some time tonight to do homework. I'm still tired from last night, maybe I can take a nap sometime today. Otherwise, I can always sleep in tomorrow. Having only afternoon classes will be interesting. My lab might be hard because it's at night, but it seems really lax. It's such a difference from the chem labs. I'm so glad it doesn't seem hard at all. I hope my physics lab is easy as well. One can only hope. Not having lab the first week is nice. It worked out well so I could be home to help my parents help move our stuff. It's such a pain to have to move when school is already in session. I'm a little shocked the manager didn't have us sign the lease before we moved in. I hope that's not an indication of how much she cares about the renters. At least there's a lady downstairs who has lived here for 20 years that knows everything about this place. It's so nice having an apartment right next to a bus stop. I think it's fate we moved in here. After all the hassles we've had it looks like its going to be worth it. I still have to hang up all my clothes, but everything else is done. I can't forget to meet Dan at 10am on Monday in the PCL to work on physics homework. It's 6 pages long. I hope it's as easy as the first homework. It still took a while to finish though. From 7pm to 1am. This time I'll start earlier. I'm glad that my sister is coming in town, but she picked a bad weekend. I have a lot of reading to do and 2 tests to study for. I need to start seriously studying the PCAT. I can't believe I almost missed the deadline on that again. This is ridiculous. I hope I get into UT Austin. That's the only place I'd really like to go. Except maybe UCSF or University of Washington. UCSF is probably too expensive. I'd want to live in San Fran when I have money so really I only want to got o Pharmacy school in Austin or Seattle. The only thing about Seattle is I hope Sylvia has gotten cleaner. I know she never cooks anymore, so probably. We'd have to get a bigger place than what she has now and I might just have to leave all my stuff here and buy new stuff. That's going to be expensive. I hope whatever happens, Mom and Dad aren't disappointed in me. They need a web page for PAC so I know when I need to get in line and for what. The draw system for the football team is so much more efficient, even though they don't give out all the information I need either. Maybe next year I won't buy either. It's so much of a pain. I don't even feel like going. I bet that's how they make their money. I don't know about the plays, but at the games students get crappy seats. I know they were trying to improve that, I wonder if that went through. Probably not. As a whole, UT just cares about money and not the poor students. That's why alumni get everything. Good luck to the ""sick"" staff."
	#	"""]

	start_time_test_nbc = time.time()
	predicted = gs_clf.predict(docs_new)
	print("\nRuntime For testing NBC : " + str(time.time() - start_time_test_nbc))
	print("Predicted Value : ")
	print predicted

def knn_grid(dataset, docs_new):
	X_train, X_test, y_train, y_test = train_test_split(dataset.data, dataset.target, test_size=0.2, random_state=0)

	clf = Pipeline([('vect', CountVectorizer(decode_error='replace')),
					('tfidf', TfidfTransformer()),
					('clf', KNeighborsClassifier()),
					])
	# clf.fit(X_train, y_train)

	parameters = {'vect__ngram_range': [(1, 1), (1, 2)],
				  'tfidf__use_idf': (True, False),
				  'clf__alpha': (1e-2, 1e-3),
				  }

	start_time_grid_knn = time.time()
	gs_clf = GridSearchCV(clf, parameters, n_jobs=-1)

	# fit the model
	gs_clf.fit(X_train, y_train)

	# simple test score
	# print clf.score(X_test, y_test)

	# 10-fold cross-validation score
	scores = cross_val_score(gs_clf, dataset.data, dataset.target, cv=10)
	print("\nRuntime For gridsearch KNN : " + str(time.time() - start_time_grid_knn))

	print("Accuracy: %0.4f (+/- %0.4f)" % (scores.mean(), scores.std() * 2))

	start_time_val_knn = time.time()
	y_pred = gs_clf.best_estimator_.predict(X_test)
	print("\nRuntime For validation KNN : " + str(time.time() - start_time_val_knn))

	print("\n\nMetrics are here : \n\n")
	print(classification_report(y_pred, y_test))
	print("\n\nConfusion Matrix : \n")
	print(confusion_matrix(y_test, y_pred))
	print("\n\n Precision score: ")
	print(precision_score(y_pred, y_test))
	print("\nRecall score: ")
	print(recall_score(y_test, y_pred))
	print("\nF1 score: ")
	print(f1_score(y_test, y_pred))
	print("\nROC Curve: ")
	roc_construct(y_test, y_pred)
	joblib.dump(gs_clf, 'nb_' + selected_trait + '.joblib')

	print "______________________"

	# single entity test
	# docs_new = ["""
	#	I have to go to the SI unit and then the Crew meeting and come home, shower, get ready, and meet Ann and Chad for dinner. I should have some time tonight to do homework. I'm still tired from last night, maybe I can take a nap sometime today. Otherwise, I can always sleep in tomorrow. Having only afternoon classes will be interesting. My lab might be hard because it's at night, but it seems really lax. It's such a difference from the chem labs. I'm so glad it doesn't seem hard at all. I hope my physics lab is easy as well. One can only hope. Not having lab the first week is nice. It worked out well so I could be home to help my parents help move our stuff. It's such a pain to have to move when school is already in session. I'm a little shocked the manager didn't have us sign the lease before we moved in. I hope that's not an indication of how much she cares about the renters. At least there's a lady downstairs who has lived here for 20 years that knows everything about this place. It's so nice having an apartment right next to a bus stop. I think it's fate we moved in here. After all the hassles we've had it looks like its going to be worth it. I still have to hang up all my clothes, but everything else is done. I can't forget to meet Dan at 10am on Monday in the PCL to work on physics homework. It's 6 pages long. I hope it's as easy as the first homework. It still took a while to finish though. From 7pm to 1am. This time I'll start earlier. I'm glad that my sister is coming in town, but she picked a bad weekend. I have a lot of reading to do and 2 tests to study for. I need to start seriously studying the PCAT. I can't believe I almost missed the deadline on that again. This is ridiculous. I hope I get into UT Austin. That's the only place I'd really like to go. Except maybe UCSF or University of Washington. UCSF is probably too expensive. I'd want to live in San Fran when I have money so really I only want to got o Pharmacy school in Austin or Seattle. The only thing about Seattle is I hope Sylvia has gotten cleaner. I know she never cooks anymore, so probably. We'd have to get a bigger place than what she has now and I might just have to leave all my stuff here and buy new stuff. That's going to be expensive. I hope whatever happens, Mom and Dad aren't disappointed in me. They need a web page for PAC so I know when I need to get in line and for what. The draw system for the football team is so much more efficient, even though they don't give out all the information I need either. Maybe next year I won't buy either. It's so much of a pain. I don't even feel like going. I bet that's how they make their money. I don't know about the plays, but at the games students get crappy seats. I know they were trying to improve that, I wonder if that went through. Probably not. As a whole, UT just cares about money and not the poor students. That's why alumni get everything. Good luck to the ""sick"" staff."
	#	"""]

	predicted = gs_clf.predict(docs_new)
	print predicted

def dt_grid(dataset, docs_new):
	X_train, X_test, y_train, y_test = train_test_split(dataset.data, dataset.target, test_size=0.2, random_state=0)

	clf = Pipeline([('vect', CountVectorizer(decode_error='replace')),
					('tfidf', TfidfTransformer()),
					('clf', DecisionTreeClassifier()),
					])
	# clf.fit(X_train, y_train)

	parameters = {'vect__ngram_range': [(1, 1), (1, 2)],
				  'tfidf__use_idf': (True, False),
				  'clf__alpha': (1e-2, 1e-3),
				  }
	start_time_grid_dt = time.time()
	gs_clf = GridSearchCV(clf, parameters, n_jobs=-1)

	# fit the model
	gs_clf.fit(X_train, y_train)

	# simple test score
	# print clf.score(X_test, y_test)

	# 10-fold cross-validation score
	scores = cross_val_score(gs_clf, dataset.data, dataset.target, cv=10)
	print("\nRuntime For gridsearch Decision Tree : " + str(time.time() - start_time_grid_dt))

	print("Accuracy: %0.4f (+/- %0.4f)" % (scores.mean(), scores.std() * 2))

	start_time_val_dt = time.time()
	y_pred = gs_clf.best_estimator_.predict(X_test)
	print("\nRuntime For Decision Tree : " + str(time.time() - start_time_val_dt))

	print("\n\nMetrics are here : \n\n")
	print(classification_report(y_pred, y_test))
	print("\n\nConfusion Matrix : \n")
	print(confusion_matrix(y_test, y_pred))
	print("\n\n Precision score: ")
	print(precision_score(y_pred, y_test))
	print("\nRecall score: ")
	print(recall_score(y_test, y_pred))
	print("\nF1 score: ")
	print(f1_score(y_test, y_pred))

	joblib.dump(gs_clf, 'nb_' + selected_trait + '.joblib')

	print "______________________"

	# single entity test
	# docs_new = ["""
	#	I have to go to the SI unit and then the Crew meeting and come home, shower, get ready, and meet Ann and Chad for dinner. I should have some time tonight to do homework. I'm still tired from last night, maybe I can take a nap sometime today. Otherwise, I can always sleep in tomorrow. Having only afternoon classes will be interesting. My lab might be hard because it's at night, but it seems really lax. It's such a difference from the chem labs. I'm so glad it doesn't seem hard at all. I hope my physics lab is easy as well. One can only hope. Not having lab the first week is nice. It worked out well so I could be home to help my parents help move our stuff. It's such a pain to have to move when school is already in session. I'm a little shocked the manager didn't have us sign the lease before we moved in. I hope that's not an indication of how much she cares about the renters. At least there's a lady downstairs who has lived here for 20 years that knows everything about this place. It's so nice having an apartment right next to a bus stop. I think it's fate we moved in here. After all the hassles we've had it looks like its going to be worth it. I still have to hang up all my clothes, but everything else is done. I can't forget to meet Dan at 10am on Monday in the PCL to work on physics homework. It's 6 pages long. I hope it's as easy as the first homework. It still took a while to finish though. From 7pm to 1am. This time I'll start earlier. I'm glad that my sister is coming in town, but she picked a bad weekend. I have a lot of reading to do and 2 tests to study for. I need to start seriously studying the PCAT. I can't believe I almost missed the deadline on that again. This is ridiculous. I hope I get into UT Austin. That's the only place I'd really like to go. Except maybe UCSF or University of Washington. UCSF is probably too expensive. I'd want to live in San Fran when I have money so really I only want to got o Pharmacy school in Austin or Seattle. The only thing about Seattle is I hope Sylvia has gotten cleaner. I know she never cooks anymore, so probably. We'd have to get a bigger place than what she has now and I might just have to leave all my stuff here and buy new stuff. That's going to be expensive. I hope whatever happens, Mom and Dad aren't disappointed in me. They need a web page for PAC so I know when I need to get in line and for what. The draw system for the football team is so much more efficient, even though they don't give out all the information I need either. Maybe next year I won't buy either. It's so much of a pain. I don't even feel like going. I bet that's how they make their money. I don't know about the plays, but at the games students get crappy seats. I know they were trying to improve that, I wonder if that went through. Probably not. As a whole, UT just cares about money and not the poor students. That's why alumni get everything. Good luck to the ""sick"" staff."
	#	"""]


	predicted = gs_clf.predict(docs_new)
	print predicted

# main program
if __name__=="__main__":
	current_directory = os.getcwd() + "/"
	data_file = current_directory + "essays.csv"

	class_labels = ['EXT','NEU','AGR','CON','OPN']

	docs_new = [""" This is to check whether the model works. It most likely does. """]

	for index, selected_trait in enumerate(class_labels):
		print("\n\n =============================================================== \n")
		print("\nSelected Trait = " + selected_trait)
		dataset = build_data_cv(data_file, index)
		print("\n SVM : \n")
		svm_grid(dataset, docs_new)
		print("\n NBC : \n")
		nbc_grid(dataset, docs_new)
		# print("\n KNN : \n")
		# knn_grid(dataset, docs_new)
		# print("\n Decision Tree : \n")
		# dt_grid(dataset, docs_new)


