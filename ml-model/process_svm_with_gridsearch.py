import csv
import os
from collections import defaultdict
import sys
import re
import numpy as np
import random
import math

import sklearn.datasets
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier
from sklearn import metrics
from sklearn.model_selection import GridSearchCV
from sklearn.externals import joblib
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import accuracy_score, confusion_matrix, roc_curve
import pickle
import matplotlib.pyplot as plt

# def plot_search_results(grid):
# 	"""
# 	Params:
# 		grid: A trained GridSearchCV object.
# 	"""
# 	## Results from grid search
# 	results = grid.cv_results_
# 	means_test = results['mean_test_score']
# 	stds_test = results['std_test_score']
# 	means_train = results['mean_train_score']
# 	stds_train = results['std_train_score']
#
# 	## Getting indexes of values per hyper-parameter
# 	masks=[]
# 	masks_names= list(grid.best_params_.keys())
# 	for p_k, p_v in grid.best_params_.items():
# 		masks.append(list(results['param_'+p_k].data==p_v))
#
# 	params=grid.param_grid
#
# 	## Ploting results
# 	fig, ax = plt.subplots(1,len(params),sharex='none', sharey='all',figsize=(20,5))
# 	fig.suptitle('Score per parameter')
# 	fig.text(0.04, 0.5, 'MEAN SCORE', va='center', rotation='vertical')
# 	pram_preformace_in_best = {}
# 	for i, p in enumerate(masks_names):
# 		m = np.stack(masks[:i] + masks[i+1:])
# 		best_parms_mask = m.all(axis=0)
# 		best_index = np.where(best_parms_mask)[0]
# 		x = np.array(params[p])
# 		y_1 = np.array(means_test[best_index])
# 		e_1 = np.array(stds_test[best_index])
# 		y_2 = np.array(means_train[best_index])
# 		e_2 = np.array(stds_train[best_index])
# 		ax[i].errorbar(x, y_1, e_1, linestyle='--', marker='o', label='test')
# 		ax[i].errorbar(x, y_2, e_2, linestyle='-', marker='^',label='train' )
# 		ax[i].set_xlabel(p.upper())
#
# 	plt.legend()
# 	plt.show()

def build_data_cv(datafile, trait_number):
	"""
	Loads data
	"""

	with open(datafile, "rb") as csvf:
		csvreader = csv.reader(csvf,delimiter=',',quotechar='"')
		data = []
		target = np.array([])

		for index, line in enumerate(csvreader):

			# escape header
			if index < 1:
				continue

			document = unicode(line[1], errors='replace')

			data.append(document)

			index_of_trait = trait_number + 2

			if line[index_of_trait].lower()=='y':
				target = np.append(target, 1.0)
			else:
				target = np.append(target, 0.0)

		dataset = sklearn.datasets.base.Bunch(data=data, target=target)
		dataset.target_names = ["positive", "negative"]

		return dataset

# main program
if __name__=="__main__":
	current_directory = os.getcwd() + "/"
	data_file = current_directory + "essays.csv"

	class_labels = ['EXT','NEU','AGR','CON','OPN']

	for index, selected_trait in enumerate(class_labels):


		dataset = build_data_cv(data_file, index)

		X_train, X_test, y_train, y_test = train_test_split(dataset.data, dataset.target, test_size=0.2, random_state=0)

		clf = Pipeline([('vect', CountVectorizer(decode_error='replace')),
					  ('tfidf', TfidfTransformer()),
					  ('clf', SGDClassifier(loss='hinge', penalty='l2',
											alpha=1e-3, random_state=42,
											max_iter=5, tol=None)),
		])
		# clf.fit(X_train, y_train)

		parameters = {'vect__ngram_range': [(1, 1), (1, 2)],
			   'tfidf__use_idf': (True, False),
			   'clf__alpha': (1e-2, 1e-3),
		}
		gs_clf = GridSearchCV(clf, parameters, n_jobs=-1)

		# fit the model
		gs_clf.fit(X_train, y_train)

		# simple test score
		# print clf.score(X_test, y_test)

		# 10-fold cross-validation score
		scores = cross_val_score(gs_clf, dataset.data, dataset.target, cv=10)
		print("Accuracy: %0.4f (+/- %0.4f)" % (scores.mean(), scores.std() * 2))
		print(gs_clf.cv_results_)
		y_pred = gs_clf.best_estimator_.predict(X_test)
		print("\n\ny pred is here : \n\n")
		print(y_pred)
		#plot_search_results(gs_clf)
		print("\n\nMetrics are here : \n\n")
		metrics.classification_report(y_pred, y_test)
		# Export the classifier to a file
		joblib.dump(gs_clf, 'svm_gs_'+selected_trait+'.joblib')
#		with open('svm_gs_'+selected_trait+'.pkl', 'wb') as f:
#			pickle.dump(gs_clf, f)


		#single entity test
		docs_new = ["""
			I have to go to the SI unit and then the Crew meeting and come home, shower, get ready, and meet Ann and Chad for dinner. I should have some time tonight to do homework. I'm still tired from last night, maybe I can take a nap sometime today. Otherwise, I can always sleep in tomorrow. Having only afternoon classes will be interesting. My lab might be hard because it's at night, but it seems really lax. It's such a difference from the chem labs. I'm so glad it doesn't seem hard at all. I hope my physics lab is easy as well. One can only hope. Not having lab the first week is nice. It worked out well so I could be home to help my parents help move our stuff. It's such a pain to have to move when school is already in session. I'm a little shocked the manager didn't have us sign the lease before we moved in. I hope that's not an indication of how much she cares about the renters. At least there's a lady downstairs who has lived here for 20 years that knows everything about this place. It's so nice having an apartment right next to a bus stop. I think it's fate we moved in here. After all the hassles we've had it looks like its going to be worth it. I still have to hang up all my clothes, but everything else is done. I can't forget to meet Dan at 10am on Monday in the PCL to work on physics homework. It's 6 pages long. I hope it's as easy as the first homework. It still took a while to finish though. From 7pm to 1am. This time I'll start earlier. I'm glad that my sister is coming in town, but she picked a bad weekend. I have a lot of reading to do and 2 tests to study for. I need to start seriously studying the PCAT. I can't believe I almost missed the deadline on that again. This is ridiculous. I hope I get into UT Austin. That's the only place I'd really like to go. Except maybe UCSF or University of Washington. UCSF is probably too expensive. I'd want to live in San Fran when I have money so really I only want to got o Pharmacy school in Austin or Seattle. The only thing about Seattle is I hope Sylvia has gotten cleaner. I know she never cooks anymore, so probably. We'd have to get a bigger place than what she has now and I might just have to leave all my stuff here and buy new stuff. That's going to be expensive. I hope whatever happens, Mom and Dad aren't disappointed in me. They need a web page for PAC so I know when I need to get in line and for what. The draw system for the football team is so much more efficient, even though they don't give out all the information I need either. Maybe next year I won't buy either. It's so much of a pain. I don't even feel like going. I bet that's how they make their money. I don't know about the plays, but at the games students get crappy seats. I know they were trying to improve that, I wonder if that went through. Probably not. As a whole, UT just cares about money and not the poor students. That's why alumni get everything. Good luck to the ""sick"" staff."
		 """]

		predicted = gs_clf.predict(docs_new)
		print predicted
#		break
